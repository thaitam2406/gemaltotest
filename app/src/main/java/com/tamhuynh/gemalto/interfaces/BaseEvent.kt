package com.tamhuynh.gemalto.interfaces

open class BaseEvent {
    internal var error: Throwable? = null
    private var requestCode: Int = 0

    constructor() : this(REQUEST_GENERAL, null)

    constructor(requestCode: Int) : this(requestCode, null)

    constructor(error: Throwable?) : this(REQUEST_GENERAL, error)

    constructor(requestCode: Int, error: Throwable?) {
        this@BaseEvent.requestCode = requestCode
        this@BaseEvent.error = error
    }

    fun getRequestCode(): Int {
        return requestCode
    }

    fun getError(): Throwable? {
        return error
    }

    fun hasError(): Boolean {
        return error != null
    }

    override fun toString(): String {
        val errorString = error?.toString() ?: "null"
        return "BaseEvent{error=$errorString}"
    }

    companion object {
        const val REQUEST_GENERAL = -1
    }
}
