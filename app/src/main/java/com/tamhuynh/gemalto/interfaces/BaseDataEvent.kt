package com.tamhuynh.gemalto.interfaces

open class BaseDataEvent<T> : BaseEvent {
    private var data: T? = null

    constructor()

    constructor(error: Throwable) : this(null, error)

    constructor(data: T) : this(data, null)

    constructor(data: T?, error: Throwable? = null) : super(error) {
        this.data = data
    }

    constructor(requestCode: Int, data: T) : super(requestCode) {
        this.data = data
    }

    constructor(requestCode: Int, error: Throwable) : super(requestCode, error)

    fun getData(): T? {
        return data
    }

    override fun toString(): String {
        return "BaseDataEvent{error=$error data=$data}"
    }
}
