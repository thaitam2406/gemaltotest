package com.tamhuynh.gemalto.interfaces

import android.os.Bundle

class NavigateScreenEvent {

    var tagScreen = ""
        private set

    var bundleData: Bundle? = null
    lateinit var toolbarTitle: String

    constructor(tagScreen: String, bundleData: Bundle, toolbarTitle: String) {
        this.tagScreen = tagScreen
        this.bundleData = bundleData
        this.toolbarTitle = toolbarTitle
    }

    constructor(tagScreen: String) {
        this.tagScreen = tagScreen
    }

}
