package com.tamhuynh.gemalto.interfaces

class MessageEvent : BaseDataEvent<String> {
    constructor(data: String) : super(data)
    constructor(throwable: Throwable) : super(throwable)
}
