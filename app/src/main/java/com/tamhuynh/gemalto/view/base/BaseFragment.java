package com.tamhuynh.gemalto.view.base;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.starhub.newmsa.views.base.INavigableActivity;
import com.starhub.newmsa.views.base.INavigableScreen;
import com.starhub.newmsa.views.base.IShowHideProgressbar;
import com.tamhuynh.gemalto.interfaces.MessageEvent;
import com.tamhuynh.gemalto.utils.LoadingUtil;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import timber.log.Timber;

import java.util.List;

/**
 * <p>Base fragment for all fragment in the program. You should subclass from
 * {@link BaseFragmentLayoutResId} or {@link BaseFragmentLayoutView} instead of directly from this
 * class.</p>
 * <p/>
 * <p>If you want to direct subclass from {@link BaseFragment}, make sure that neither
 * {@link #getContentResId()} or {@link #getContentView()} returns the real content view of the
 * fragment. The other must return {@code View.NO_ID} (for {@link #getContentResId()}) or
 * {@code null} (for {@link #getContentView()}).</p>
 */

public abstract class BaseFragment extends Fragment implements INavigableScreen, IShowHideProgressbar {

    public Dialog dialog;
    private View mRootView;
    private String TAG = "";

    public void setTag(String tag) {
        TAG = tag;
    }


    public BaseFragment() {
    }

    /**
     * Return the resId for the fragment content view. Return {@code View.NO_ID} when fragment
     * view is given by {@link #getContentView()} .
     */
    public abstract int getContentResId();

    /**
     * Return the fragment content view. This method is only called when {@link #getContentResId()}
     * returns {@code View.NO_ID}.
     */
    public abstract View getContentView();

    public INavigableActivity getNavigableActivity() {
        return (INavigableActivity) getActivity();
    }

    protected abstract boolean isShowProgressBarInit();

    /*
     * Get the root view of current fragment
     *
     * @return root view
     */
//    protected View getRootView() {
//        return mRootView;
//    }

    @Override
    public void hideProgressBar() {
        if (dialog != null)
            dialog.cancel();
    }

    /**
     * Init the task before render UI in Background
     */
    public abstract void initLogicBackground();

    public abstract void initData();

    /**
     * Init the UI after activity created.
     */
    public abstract void initUI();

    Observable<Object> myObservable = Observable.create(emitter -> {
                try {
                    //doing some task in background if any before renderUI
                    initLogicBackground();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                emitter.onNext("");
                emitter.onComplete();
            }
    );

    private long startTime = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        startTime = System.currentTimeMillis();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public abstract void onConfigurationChanged();

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        onConfigurationChanged();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        TAG = setTag();
        String className = getClass().getName();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            int xml = getContentResId();
            if (xml != View.NO_ID) {
                mRootView = inflater.inflate(xml, container, false);
            } else {
                mRootView = getContentView();
            }
            dialog = LoadingUtil.INSTANCE.createProgressDialog(getActivity(), false, null);
            if (isShowProgressBarInit())
                showProgressBar();
        }

        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        long endTime = System.currentTimeMillis();
        long differentTime = endTime - startTime;
        long timeInit = (differentTime / 1000) % 60;
        String str = String.valueOf(differentTime);
        Log.d("TAG", TAG);
        Timber.i(str);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        handleInit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dialog = null;
        mRootView = null;
        //ButterKnife.unbind(this);
    }

    @Subscribe
    public void onMessageEvent(MessageEvent messageEvent) {
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            EventBus.getDefault().register(this);
        } catch (Exception ignored) {

        }

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            EventBus.getDefault().unregister(this);
        } catch (Exception ignored) {
        }
    }

    @Override
    public void showProgressBar() {
        if (dialog != null)
            dialog.show();
    }

    /**
     * Base Fragment with layout inflated from a xml resource.
     */
    public static abstract class BaseFragmentLayoutResId extends BaseFragment {
        @Override
        final public View getContentView() {
            return null;
        }

       final public void setOnClick(List<View> views, View.OnClickListener onClickListener) {
            if (views != null)
                for (View v : views)
                    if (v != null)
                        v.setOnClickListener(onClickListener);
        }

       final public void setOnClick(View.OnClickListener onClickListener, View... views) {
            if (views != null)
                for (View v : views)
                    if (v != null)
                        v.setOnClickListener(onClickListener);
        }
    }

    /**
     * Base Fragment with layout created from a {@link View}.
     */
    public static abstract class BaseFragmentLayoutView extends BaseFragment {
        @Override
        final public int getContentResId() {
            return View.NO_ID;
        }
    }

    public void handleInit() {
        myObservable.subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(new Observer<Object>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Object o) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        initData();
                        initUI();
                    }

                });
    }
}