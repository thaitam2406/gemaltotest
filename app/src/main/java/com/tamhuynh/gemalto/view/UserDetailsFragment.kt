package com.tamhuynh.gemalto.view

import android.widget.Toast
import androidx.lifecycle.Observer
import com.tamhuynh.gemalto.Injection
import com.tamhuynh.gemalto.R
import com.tamhuynh.gemalto.view.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_user_detail.*
import tamhuynh.gemalto.mylibrary.db.UserEntity

class UserDetailsFragment : BaseFragment.BaseFragmentLayoutResId() {

    private var mainViewModel: MainViewModel? = null
    var isStored = false
    var comeFrom: String = ""
    lateinit var userEntity : UserEntity

    override fun getContentResId() = R.layout.fragment_user_detail

    override fun isShowProgressBarInit() = true

    override fun initLogicBackground() {
    }

    override fun initData() {
        var gender : String = arguments?.get("Gender").toString()
        var userID : String = arguments?.get("UserId").toString()
        comeFrom = arguments?.get("ComeFrom").toString()
        mainViewModel = Injection.provideViewModelFactory(requireContext()).create(MainViewModel::class.java)
        if(gender!= null && !gender.isEmpty() && !gender.equals("null")) {
            mainViewModel?.getUserViaGender(gender)
                ?.observe(this@UserDetailsFragment, Observer { user ->
                    hideProgressBar()
                    bindUserDetails(user)
                })
        }else{
            mainViewModel?.getUserViaSeed(userID)
                ?.observe(this@UserDetailsFragment, Observer { user ->
                    hideProgressBar()
                    bindUserDetails(user)
                })
        }
    }

    private fun bindUserDetails(user: UserEntity) {
        userEntity = user
        tvId.text = user.id
        tvName.text = user.userName
        tvGender.text = user.gender
        tvAge.text = user.age
        tvDob.text = user.dob
        tvEmail.text = user.email
    }

    override fun initUI() {
        if(QueryUserFragment::class.java.name.equals(comeFrom)) {
            isStored = true;
            btnAction.text = "Store"
        }
        btnAction.setOnClickListener {
            if(isStored){
                mainViewModel?.storedUser(userEntity)
                    ?.observe(this@UserDetailsFragment, Observer {isStored ->
                        if(isStored) {
                            Toast.makeText(requireContext(), "Stored is success", Toast.LENGTH_LONG).show()
                        }else{
                            Toast.makeText(requireContext(), "Stored is failed", Toast.LENGTH_LONG).show()
                        }
                    })
            }else{
                mainViewModel?.deleteUser(userEntity)
                    ?.observe(this@UserDetailsFragment, Observer {isStored ->
                        if(isStored) {
                            Toast.makeText(requireContext(), "Delete is success", Toast.LENGTH_LONG).show()
                        }else{
                            Toast.makeText(requireContext(), "Delete is failed", Toast.LENGTH_LONG).show()
                        }
                    })
            }
        }
    }

    override fun onConfigurationChanged() {
    }
}