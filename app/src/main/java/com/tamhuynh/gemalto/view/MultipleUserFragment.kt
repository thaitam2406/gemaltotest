package com.tamhuynh.gemalto.view

import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.tamhuynh.gemalto.Injection
import com.tamhuynh.gemalto.R
import com.tamhuynh.gemalto.view.adapter.UserStoredAdapter
import com.tamhuynh.gemalto.view.base.BaseFragment
import com.tamhuynh.gemalto.view.base.DividerItemDecoration
import kotlinx.android.synthetic.main.fragment_users_stored.*
import tamhuynh.gemalto.mylibrary.db.UserEntity

/**
 * Created by TamHuynh on 14,March,2019
 */
class MultipleUserFragment : BaseFragment.BaseFragmentLayoutResId() {

    private var mainViewModel: MainViewModel? = null
    private  var userEntities : ArrayList<UserEntity> = ArrayList()
    lateinit var mAdapter : UserStoredAdapter

    override fun getContentResId() = R.layout.fragment_users_stored

    override fun isShowProgressBarInit() = true

    override fun initLogicBackground() {
    }

    override fun initData() {
        mAdapter = UserStoredAdapter(userEntities)
        val mLayoutManager = LinearLayoutManager(context?.applicationContext)
        rcvUserStored.layoutManager = mLayoutManager
        rcvUserStored.itemAnimator = DefaultItemAnimator()
        rcvUserStored.adapter = mAdapter
        rcvUserStored.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                R.drawable.divider_default,
                LinearLayoutManager.VERTICAL
            )
        )
        mainViewModel = Injection.provideViewModelFactory(requireContext())
            .create(MainViewModel::class.java)

        var multipleUser : String = arguments?.getString("multipleUser").toString()
        if(multipleUser!= null && !multipleUser.equals("null") && !multipleUser.isEmpty()){
            mainViewModel?.getUserList(multipleUser.toInt())
                ?.observe(this@MultipleUserFragment, Observer { list ->
                    hideProgressBar()
                    userEntities.addAll(list)
                    mAdapter.notifyDataSetChanged()
                })
        }else {
            mainViewModel?.getStoredUserList()
                ?.observe(this@MultipleUserFragment, Observer { list ->
                    hideProgressBar()
                    userEntities.addAll(list)
                    mAdapter.notifyDataSetChanged()
                })
        }
    }

    override fun initUI() {
    }

    override fun onConfigurationChanged() {
    }

    public fun storedMultipeUser(){
        mainViewModel?.storedMultipleUser(userEntities)
            ?.observe(this@MultipleUserFragment, Observer {
                Toast.makeText(requireContext(), "Stored is succeess", Toast.LENGTH_LONG).show()
            })
    }

    public fun deleteAllUser(){
        mainViewModel?.deleteAllUser()
            ?.observe(this@MultipleUserFragment, Observer {
                Toast.makeText(requireContext(), "Delete is succeess", Toast.LENGTH_LONG).show()
                activity?.supportFragmentManager?.popBackStack()
            })
    }
}