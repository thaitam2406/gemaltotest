package com.tamhuynh.gemalto.view.base

import android.os.Bundle
import androidx.fragment.app.Fragment

object FragmentFactory {
    @Throws(Exception::class)
    fun createFragment(tag: String, bundle: Bundle) = createFragment(tag).apply {
        arguments = bundle
    }

    @Throws(Exception::class)
    private fun createFragment(tag: String): Fragment {
        try {
            val tags = tag.split("_")
            return Class.forName(tags[0]).newInstance() as Fragment
        } catch (e: InstantiationException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        }

        throw Exception("Fragment name is not exist")
    }
}
