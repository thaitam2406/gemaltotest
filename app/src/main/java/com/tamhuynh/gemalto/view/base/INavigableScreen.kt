package com.starhub.newmsa.views.base

interface INavigableScreen {
    val navigableActivity: INavigableActivity
}
