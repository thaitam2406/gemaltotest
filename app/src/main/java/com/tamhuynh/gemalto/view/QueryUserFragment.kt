package com.tamhuynh.gemalto.view

import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import com.tamhuynh.gemalto.R
import com.tamhuynh.gemalto.interfaces.NavigateScreenEvent
import com.tamhuynh.gemalto.view.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_query_users.*
import org.greenrobot.eventbus.EventBus

class QueryUserFragment : BaseFragment.BaseFragmentLayoutResId() {

    val listGender: ArrayList<String> = ArrayList()
    override fun getContentResId() = R.layout.fragment_query_users

    override fun isShowProgressBarInit() = false

    override fun initLogicBackground() {
    }

    override fun initData() {
    }

    override fun initUI() {
        listGender.add("")
        listGender.add("Male")
        listGender.add("Female")
        var mGenderArrayAdapter = ArrayAdapter(
            context,
            R.layout.simple_dropdown_item_1line,listGender
        )
        mGenderArrayAdapter?.setDropDownViewResource(android.R.layout.simple_list_item_1)
        spinnerGender.adapter = mGenderArrayAdapter

        btnQuery.setOnClickListener {
            var gender : String = listGender.get(spinnerGender.selectedItemPosition)
            var userID : String = edUserID.text.toString()
            var multipleUser : String = edtMultipleUser.text.toString()
            if(gender.isEmpty() && userID.isEmpty() && multipleUser.isEmpty()){
                Toast.makeText(context, "Please select either gender or input user id or Multipe User", Toast.LENGTH_LONG).show()
                edUserID.error = getString(R.string.required_input)
                edtMultipleUser.error = getString(R.string.required_input)
            }else if(!userID.isEmpty() || !gender.isEmpty()){
                val bundle = Bundle()
                bundle.putString("ComeFrom", QueryUserFragment::class.java.name)
                if(!gender.isEmpty())
                    bundle.putString("Gender", gender)
                else if(!userID.isEmpty())
                    bundle.putString("UserId", userID)
                EventBus.getDefault().post(
                    NavigateScreenEvent(
                        UserDetailsFragment::class.java.name,
                        bundle, resources.getString(R.string.query_user)
                    )
                )
            }else if(!multipleUser.isEmpty()){
                val bundle = Bundle()
                bundle.putString("ComeFrom", QueryUserFragment::class.java.name)
                bundle.putString("multipleUser", multipleUser)
                EventBus.getDefault().post(
                    NavigateScreenEvent(
                        MultipleUserFragment::class.java.name,
                        bundle, resources.getString(R.string.query_multi_user)
                    )
                )
            }
        }
    }

    override fun onConfigurationChanged() {
    }
}