package com.starhub.newmsa.views.base

interface IShowHideProgressbar {

    fun hideProgressBar()

    fun showProgressBar()
}
