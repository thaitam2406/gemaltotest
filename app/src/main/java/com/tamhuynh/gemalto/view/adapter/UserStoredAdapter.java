package com.tamhuynh.gemalto.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tamhuynh.gemalto.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import tamhuynh.gemalto.mylibrary.db.UserEntity;

/**
 * Created by TamHuynh on 14,March,2019
 */
public class UserStoredAdapter extends RecyclerView.Adapter<UserStoredAdapter.MyViewHolder> {

    ArrayList<UserEntity> userEntities;

    public UserStoredAdapter(ArrayList<UserEntity> userEntities) {
        this.userEntities = userEntities;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user_stored, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        UserEntity userEntity = userEntities.get(position);
        holder.name.setText(userEntity.getUserName());
        holder.gender.setText(userEntity.getGender());
        holder.email.setText(userEntity.getEmail());
    }

    @Override
    public int getItemCount() {
        return userEntities.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, gender, email;

        public MyViewHolder(View view) {
            super(view);
            name =  view.findViewById(R.id.tvName);
            gender =  view.findViewById(R.id.tvGender);
            email =  view.findViewById(R.id.tvEmail);
        }
    }
}
