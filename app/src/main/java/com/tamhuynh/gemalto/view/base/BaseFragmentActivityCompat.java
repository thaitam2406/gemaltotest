package com.tamhuynh.gemalto.view.base;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.starhub.newmsa.views.base.INavigableActivity;
import com.tamhuynh.gemalto.MyApplication;
import com.tamhuynh.gemalto.R;
import com.tamhuynh.gemalto.interfaces.MessageEvent;
import com.tamhuynh.gemalto.utils.ConnectivityReceiver;
import com.tamhuynh.gemalto.utils.DialogUtils;
import com.tamhuynh.gemalto.utils.LoadingUtil;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import timber.log.Timber;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class BaseFragmentActivityCompat extends AppCompatActivity implements
        FragmentManager.OnBackStackChangedListener, INavigableActivity, ConnectivityReceiver.IConnectivityReceiverListener {
    protected ArrayList<String> NAVIGABLE_FRAGMENTS = new ArrayList<>();
    protected FragmentManager mFragmentManager;
    @Nullable
    Toolbar mToolbar;
    private String mCurrentFragmentTag = "";
    public HashMap<String, String> mTitleMap = new HashMap<>();

    protected Bundle bundle;
    public boolean isNetworkActive = true;
    public Dialog dialog;

    protected abstract boolean isShowProgressBarInit();

    public abstract int getLayoutRedId();

    protected abstract int getFragmentContainerResId();

    public abstract void initData();

    public abstract void initUI();

    /**
     * Mark navigable fragments via call {@link #markNavigableFragment(String)}
     */
    protected abstract void initNavigationFragment();

    /**
     * Init toolbar title for each fragment
     */
    protected abstract void initTitleToolBar(String title);

    public abstract void updateToolbar(String currentFragment, String s);

    /**
     * Init the task before render UI in Background
     */
    public abstract void initLogicBackground();

    Observable<String> myObservable = Observable.create(emitter -> {
                initLogicBackground();
                emitter.onNext("");
                emitter.onComplete();
            }
    );

    protected BaseFragmentActivityCompat getActivity() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutRedId());
        bundle = savedInstanceState;
        EventBus.getDefault().register(this);
        mFragmentManager = getSupportFragmentManager();
        mFragmentManager.addOnBackStackChangedListener(this);

        initNavigationFragment();
        initTitleToolBar("");

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
        myObservable.subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(s -> {
                    initData();
                    initUI();
                });
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        dialog = LoadingUtil.INSTANCE.createProgressDialog(getActivity(), false, null);
        if (isShowProgressBarInit())
            showProgressBar();

    }

    public void addFragment(int containerViewId, String fragmentTag, Bundle data) {
        if (fragmentTag.equals(mCurrentFragmentTag)) {
            return;
        }
        // check if the fragment added, pop from BackStack
        Fragment frag = mFragmentManager.findFragmentByTag(fragmentTag);
        if (frag != null) {
            if (NAVIGABLE_FRAGMENTS.contains(fragmentTag)) {
                mFragmentManager.popBackStack(fragmentTag, 0);
                frag.onResume();
                return;
            }
        }

        // Create new fragment
        try {
            frag = FragmentFactory.INSTANCE.createFragment(fragmentTag, data);
        } catch (Exception e) {
            Timber.d(e);
        }
        if (frag != null) {
            mCurrentFragmentTag = fragmentTag;
            mFragmentManager.beginTransaction()
                    .add(containerViewId, frag, fragmentTag)
                    .addToBackStack(fragmentTag)
                    .commit();
        }
    }

    public String getCurrentFragmentTag() {
        return mCurrentFragmentTag;
    }

    public void onConfigurationChanged() {
        //Ensure toolbar update when the orientation changed
        updateToolbar(getCurrentFragmentTag(), mTitleMap.get(getCurrentFragmentTag()));
    }

    public void markNavigableFragment(String tag) {
        NAVIGABLE_FRAGMENTS.add(tag);
    }

    public void setmCurrentFragmentTag(String mCurrentFragmentTag) {
        this.mCurrentFragmentTag = mCurrentFragmentTag;
    }

    public void showAlert(boolean isConnected, boolean isFirstTimeCheckNetwork) {
        Context context = this;
        if (isConnected) {
            MyApplication.Companion.setShowDialog(0);
            /**
             * if the first time check internet connection, don't need to do anything
             */
        } else {
            if (!com.tamhuynh.gemalto.MyApplication.Companion.isConnected() && com.tamhuynh.gemalto.MyApplication.Companion.isShowDialog() == 0) {
                DialogUtils.showCommonDialog(context, context.getString(R.string.network_error), context.getString(R.string.ok), null);
                com.tamhuynh.gemalto.MyApplication.Companion.setShowDialog(1);
            }
        }
    }

    @Override
    public void onBackPressed() {
         doBackPressed();
    }

    private void doBackPressed() {
        int fragmentCount = mFragmentManager.getBackStackEntryCount();
        if (fragmentCount > 1) {
            mFragmentManager.popBackStack();
        } else {
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return false;
    }


    @Override
    public void onBackStackChanged() {
        if (mFragmentManager.getBackStackEntryCount() >= 1) {
            String temp = mFragmentManager.getBackStackEntryAt(
                    mFragmentManager.getBackStackEntryCount() - 1).getName();
            /** update tool bar **/
            mCurrentFragmentTag = temp;
            updateToolbar(mCurrentFragmentTag, mTitleMap.get(mCurrentFragmentTag));
        }
    }

    @Override
    public void showScreen(String screenTag, Bundle data) {
        addFragment(getFragmentContainerResId(), screenTag, data);
    }

    @Override
    public void showScreen(String activityTag, Bundle data, boolean isBackable) {
    }

    @Subscribe
    public void onMessageEvent(MessageEvent messageEvent) {
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isNetworkActive != isConnected) {
            isNetworkActive = isConnected;
            if (!isConnected)
                showAlert(isConnected, false);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        onConfigurationChanged();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ConnectivityReceiver.Companion.removeListenerNetwork(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            Timber.d(e);
        }
        ConnectivityReceiver.Companion.addListenerNetwork(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            EventBus.getDefault().unregister(this);
        } catch (Exception e) {
            Timber.d(e);
        }
        ConnectivityReceiver.Companion.removeListenerNetwork(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void hideProgressBar() {
        if (dialog != null)
            dialog.cancel();
    }

    public void showProgressBar() {
        if (dialog != null)
            dialog.show();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }
}
