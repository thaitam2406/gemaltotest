/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tamhuynh.gemalto.view.base

import android.content.Context
import android.graphics.Canvas
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tamhuynh.gemalto.R

class DividerItemDecoration(context: Context, dividerResId: Int? = null, orientation: Int? = null) : RecyclerView.ItemDecoration() {

    private var mDivider: Drawable? = null
    private var mOrientation: Int = 0
    private var skipLastItem = false

    // Make the ViewHolder class implement this interface to skip drawing a separator
    interface SkipDecorationViewHolder

    init {
        mDivider = ContextCompat.getDrawable(context, dividerResId?.let {
            dividerResId
        } ?: kotlin.run {
            R.drawable.divider_default
        })

        orientation?.let {
            setOrientation(it)
        } ?: kotlin.run {
            setOrientation(VERTICAL_LIST)
        }
    }

    fun setColor(color: Int) {
        mDivider?.let {
            it.colorFilter =    PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN)
        }
    }

    fun setSkipLastItem(skip: Boolean) {
        skipLastItem = skip
    }

    fun setOrientation(orientation: Int) {
        if (orientation != HORIZONTAL_LIST && orientation != VERTICAL_LIST) {
            throw IllegalArgumentException("invalid orientation")
        }
        mOrientation = orientation
    }

    fun skipDecoration(view: View, parent: RecyclerView): Boolean {
        if (skipLastItem) {
            val adapter = parent.adapter
            if (adapter != null) {
                val total = adapter.itemCount
                val adapterPosition = parent.getChildAdapterPosition(view)
                if (adapterPosition == total - 1) {
                    return true
                }
            }
        }
        val holder = parent.getChildViewHolder(view)
        return holder is SkipDecorationViewHolder
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDraw(c, parent, state)
        if (mOrientation == VERTICAL_LIST) {
            drawVertical(c, parent)
        } else {
            drawHorizontal(c, parent)
        }
    }

    fun drawVertical(c: Canvas, parent: RecyclerView) {
        val left = parent.paddingLeft
        val right = parent.width - parent.paddingRight

        val childCount = parent.childCount
        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)
            if (skipDecoration(child, parent)) {
                continue
            }
            val params = child
                .layoutParams as RecyclerView.LayoutParams
            val top = child.bottom + params.bottomMargin +
                Math.round(ViewCompat.getTranslationY(child))
            val bottom = top + mDivider!!.intrinsicHeight
            mDivider!!.setBounds(left, top, right, bottom)
            mDivider!!.draw(c)
        }
    }

    fun drawHorizontal(c: Canvas, parent: RecyclerView) {
        val top = parent.paddingTop
        val bottom = parent.height - parent.paddingBottom

        val childCount = parent.childCount
        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)
            if (skipDecoration(child, parent)) {
                continue
            }
            val params = child
                .layoutParams as RecyclerView.LayoutParams
            val left = child.right + params.rightMargin +
                Math.round(ViewCompat.getTranslationX(child))
            val right = left + mDivider!!.intrinsicHeight
            mDivider!!.setBounds(left, top, right, bottom)
            mDivider!!.draw(c)
        }
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        if (mOrientation == VERTICAL_LIST) {
            outRect.set(0, 0, 0, mDivider!!.intrinsicHeight)
        } else {
            outRect.set(0, 0, mDivider!!.intrinsicWidth, 0)
        }
    }

    companion object {

        val HORIZONTAL_LIST = LinearLayoutManager.HORIZONTAL

        val VERTICAL_LIST = LinearLayoutManager.VERTICAL
    }
}