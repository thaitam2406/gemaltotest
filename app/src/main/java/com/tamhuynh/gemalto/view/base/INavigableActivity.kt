package com.starhub.newmsa.views.base

import android.os.Bundle

interface INavigableActivity {
    fun showScreen(screenTag: String, data: Bundle)

    fun showScreen(activityTag: String, data: Bundle, isBackable: Boolean)
}
