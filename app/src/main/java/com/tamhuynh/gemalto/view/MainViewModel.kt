package com.tamhuynh.gemalto.view

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import tamhuynh.gemalto.mylibrary.db.UserEntity

/**
 * Created by TamHuynh on 13,March,2019
 */
class MainViewModel : ViewModel(), LifecycleObserver {

    var mainRepository =  MainRepository()

    fun getUserList(count: Int) = mainRepository.getUserList(count)

    fun getStoredUserList() = mainRepository.getStoredUserList()

    fun getUserViaGender(gender: String) = mainRepository.getUserViaGender(gender)

    fun getUserViaSeed(seed: String) = mainRepository.getUserViaSeed(seed)

    fun storedUser(userEntity: UserEntity ) = mainRepository.storedUser(userEntity)

    fun storedMultipleUser(listUserEntity : List<UserEntity> ) = mainRepository.storedMultipleUser(listUserEntity)

    fun deleteUser(userEntity: UserEntity) = mainRepository.deleteUser(userEntity)

    fun deleteAllUser() = mainRepository.deleteAllUser()

}