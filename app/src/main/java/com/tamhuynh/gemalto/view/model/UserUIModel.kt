package com.tamhuynh.gemalto.view.model

/**
 * Created by TamHuynh on 13,March,2019
 */
data class UserUIModel (
    val userID: String,
    val name: String,
    val gender: String,
    val dbo: String,
    val email: String
)