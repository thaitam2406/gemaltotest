package com.tamhuynh.gemalto.view

import androidx.lifecycle.LiveData
import com.tamhuynh.gemalto.view.model.UserUIModel
import tamhuynh.gemalto.mylibrary.db.UserEntity

/**
 * Created by TamHuynh on 13,March,2019
 */
interface IMainRequest {

    fun getUserList(count : Int): LiveData<List<UserEntity>>

    fun getStoredUserList(): LiveData<List<UserEntity>>

    fun getUserViaGender(gender: String): LiveData<UserEntity>

    fun getUserViaSeed(seed : String): LiveData<UserEntity>

    fun storedUser(userEntity: UserEntity) : LiveData<Boolean>

    fun storedMultipleUser(listUsersEntity: List<UserEntity>): LiveData<Boolean>

    fun deleteUser(userEntity: UserEntity) : LiveData<Boolean>

    fun deleteAllUser () : LiveData<Boolean>
}