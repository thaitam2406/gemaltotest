package com.tamhuynh.gemalto.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.tamhuynh.gemalto.MyApplication
import com.tamhuynh.gemalto.view.model.UserUIModel
import tamhuynh.gemalto.mylibrary.db.UserEntity
import tamhuynh.gemalto.mylibrary.interfaces.GetUserStoredCallBack
import tamhuynh.gemalto.mylibrary.interfaces.StoredDeleteUserCallback
import tamhuynh.gemalto.mylibrary.network.NetworkModule
import tamhuynh.gemalto.mylibrary.network.job.GetUserCalback
import tamhuynh.gemalto.mylibrary.network.model.user.UserModel

/**
 * Created by TamHuynh on 13,March,2019
 */
class MainRepository : IMainRequest {

    var serviceWrapper = NetworkModule.getInstance(MyApplication.appContext()).serviceWrapper

    override fun getUserList(count: Int): LiveData<List<UserEntity>> {
        val data = MutableLiveData<List<UserEntity>>()
        serviceWrapper.getListUser(count, object : GetUserCalback {
            override fun onSuccess(t: UserModel) {
                var array = ArrayList<UserEntity>()
                t.results.forEachIndexed {index, it ->
                    var userEntity : UserEntity = convertUserEntity(t,index)
                    array.add(userEntity)
                }
                data.value = array
            }

            override fun onError(e: Throwable?) {
            }

        })
        return data
    }

    override fun getStoredUserList(): LiveData<List<UserEntity>> {
        val data = MutableLiveData<List<UserEntity>>()
        serviceWrapper.getListUserStored(object : GetUserStoredCallBack{
            override fun onListUser(userEntities: List<UserEntity>) {
                /*userEntities.forEach { userEntity ->
                    data.value?.add(userEntity)
                }*/
                data.value = userEntities
            }

            override fun onError() {
            }
        })
        return data
    }

    override fun getUserViaGender(gender: String): LiveData<UserEntity> {
        val data = MutableLiveData<UserEntity>()
        serviceWrapper.getUserViaGender(gender, object : GetUserCalback {
            override fun onSuccess(t: UserModel) {
                data.value = convertUserEntity(t, 0)
            }

            override fun onError(e: Throwable?) {
            }

        })
        return data
    }

    override fun getUserViaSeed(seed: String): LiveData<UserEntity> {
        val data = MutableLiveData<UserEntity>()
        serviceWrapper.getUserViaSeed(seed, object : GetUserCalback {
            override fun onSuccess(t: UserModel) {
                data.value = convertUserEntity(t, 0)
            }

            override fun onError(e: Throwable?) {
            }

        })
        return data
    }

    override fun storedUser(userEntity: UserEntity): LiveData<Boolean> {
        val data = MutableLiveData<Boolean>()
        serviceWrapper.storedUser(userEntity, object : StoredDeleteUserCallback{
            override fun onSuccess() {
                data.value = true
            }

            override fun onFail() {
                data.value = false
            }
        })
        return data;
    }

    override fun storedMultipleUser(listUsersEntity: List<UserEntity>): LiveData<Boolean> {
        val data = MutableLiveData<Boolean>()
        serviceWrapper.storedMultipleUser(listUsersEntity, object : StoredDeleteUserCallback{
            override fun onSuccess() {
                data.value = true
            }

            override fun onFail() {
                data.value = false
            }
        })
        return data;
    }

    override fun deleteUser(userEntity: UserEntity): LiveData<Boolean> {
        val data = MutableLiveData<Boolean>()
        serviceWrapper.deleteUser(userEntity, object : StoredDeleteUserCallback{
            override fun onSuccess() {
                data.value = true
            }

            override fun onFail() {
                data.value = false
            }
        })
        return data;
    }

    override fun deleteAllUser(): LiveData<Boolean> {
        val data = MutableLiveData<Boolean>()
        serviceWrapper.deleteAllUser(object : StoredDeleteUserCallback{
            override fun onSuccess() {
                data.value = true
            }

            override fun onFail() {
                data.value = false
            }
        })
        return data;
    }

    private fun convertUserEntity(t: UserModel, index: Int): UserEntity {
        return UserEntity(t.getUserID(index), t.getUserName(index),
            t.getUsergender(index), t.getUserAge(index), t.getUserDOB(index), t.getUserEmail(index), "")
    }
}