package com.tamhuynh.gemalto.utils

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import com.tamhuynh.gemalto.R

object LoadingUtil {

    fun createProgressDialog(
        activity: Activity,
        cancelable: Boolean,
        onCancelListener: DialogInterface.OnCancelListener?
    ): Dialog {
        val dialog = Dialog(activity)
        dialog.setCanceledOnTouchOutside(cancelable)
        dialog.setCancelable(cancelable)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.layout_dialog_processing)
        if (null != dialog.window)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setOnCancelListener(onCancelListener)
        return dialog
    }

    fun showProgressDialogUtil(
        activity: Activity,
        cancelable: Boolean,
        onCancelListener: DialogInterface.OnCancelListener?
    ): Dialog {
        val dialog = Dialog(activity)
        dialog.setCanceledOnTouchOutside(cancelable)
        dialog.setCancelable(cancelable)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.layout_dialog_processing)
        if (null != dialog.window)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
        dialog.setOnCancelListener(onCancelListener)
        return dialog
    }
}
