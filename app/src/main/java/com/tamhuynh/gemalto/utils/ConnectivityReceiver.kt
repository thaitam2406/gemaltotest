package com.tamhuynh.gemalto.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.AsyncTask
import com.tamhuynh.gemalto.MyApplication
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL

class ConnectivityReceiver() : BroadcastReceiver() {

    //    override fun onReceive(context: Context, arg1: Intent) {
//        CheckNetTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
//    }
//
    class CheckNetTask : AsyncTask<Void, Void, Boolean>() {
        override fun doInBackground(vararg params: Void): Boolean? {
            var myUrl: URL?
            var isConnected = false
            try {
                myUrl = URL("https://google.com")
                val connection = myUrl.openConnection()
                connection.connectTimeout = 30000
                connection.connect()
                isConnected = true
                MyApplication.isConnected = true
            } catch (e: MalformedURLException) {
                MyApplication.isConnected = false
                e.printStackTrace()
            } catch (e: IOException) {
                MyApplication.isConnected = false
                e.printStackTrace()
            }

            return isConnected
        }

        override fun onPostExecute(aBoolean: Boolean?) {
            super.onPostExecute(aBoolean)
            if (listConnectivityReceiverListeners != null) {
                for (connectivityReceiver in listConnectivityReceiverListeners!!) {
                    connectivityReceiver.onNetworkConnectionChanged(aBoolean!!)
                }
            }
        }
    }

    //
//    interface IConnectivityReceiverListener {
//        fun onNetworkConnectionChanged(isConnected: Boolean)
//    }
//
    companion object {

        var listConnectivityReceiverListeners: ArrayList<IConnectivityReceiverListener>? = null

        fun removeListenerNetwork(connectivityReceiverListener: IConnectivityReceiverListener) {
            if (null != listConnectivityReceiverListeners && listConnectivityReceiverListeners!!.contains(
                    connectivityReceiverListener
                )
            ) {
                listConnectivityReceiverListeners!!.remove(connectivityReceiverListener)
            }
        }

        fun addListenerNetwork(connectivityReceiverListener: IConnectivityReceiverListener?) {
            if (null == listConnectivityReceiverListeners) {
                listConnectivityReceiverListeners = ArrayList()
            }
            if (null != connectivityReceiverListener && !listConnectivityReceiverListeners!!.contains(
                    connectivityReceiverListener
                )
            ) {
                listConnectivityReceiverListeners!!.add(connectivityReceiverListener)
            }
        }
    }

    private var mConnectivityReceiverListener: IConnectivityReceiverListener? = null

    constructor(mConnectivityReceiverListener: IConnectivityReceiverListener?) : this() {
        this.mConnectivityReceiverListener = mConnectivityReceiverListener
    }

    override fun onReceive(context: Context, intent: Intent) {
        mConnectivityReceiverListener?.onNetworkConnectionChanged(isConnected(context))
    }

    private fun isConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    interface IConnectivityReceiverListener {
        fun onNetworkConnectionChanged(isConnected: Boolean)
    }
}
