package com.tamhuynh.gemalto.utils

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import com.tamhuynh.gemalto.R
import kotlinx.android.synthetic.main.common_dialog.view.*

class DialogUtils {
    companion object {
        @JvmStatic
        fun showCommonDialog(
            context: Context,
            messageContent: String?,
            strPositive: String?,
            positiveClickListener: View.OnClickListener?
        ): Dialog {
            return createCommonDialog(
                context,
                context.getString(R.string.app_name),
                messageContent,
                strPositive,
                null,
                null,
                positiveClickListener,
                null,
                null,
                false
            )
        }

        @JvmStatic
        fun showCommonDialog(
            context: Context,
            messageContent: String?,
            strPositive: String?,
            strNegative: String?,
            positiveClickListener: View.OnClickListener?,
            negativeClickListener: View.OnClickListener?
        ): Dialog {
            return createCommonDialog(
                context,
                context.getString(R.string.app_name),
                messageContent,
                strPositive,
                strNegative,
                null,
                positiveClickListener,
                negativeClickListener,
                null,
                false
            )
        }

        @JvmStatic
        fun showCommonDialogWithoutTitle(
            context: Context,
            messageContent: String?,
            strPositive: String?,
            strNegative: String?,
            positiveClickListener: View.OnClickListener?,
            negativeClickListener: View.OnClickListener?
        ): Dialog {
            return createCommonDialog(
                context,
                "",
                messageContent,
                strPositive,
                strNegative,
                null,
                positiveClickListener,
                negativeClickListener,
                null,
                false
            )
        }

        @JvmStatic
        fun showCommonDialogWithoutTitle(
            context: Context,
            messageContent: String?,
            strPositive: String?,
            positiveClickListener: View.OnClickListener?
        ): Dialog {
            return createCommonDialog(
                context,
                "",
                messageContent,
                strPositive,
                null,
                null,
                positiveClickListener,
                null,
                null,
                false
            )
        }

        @JvmStatic
        fun showCommonDialogWithTitle(
            context: Context,
            title: String?,
            messageContent: String?,
            strPositive: String?,
            positiveClickListener: View.OnClickListener?
        ): Dialog {
            return createCommonDialog(
                context,
                title,
                messageContent,
                strPositive,
                null,
                null,
                positiveClickListener,
                null,
                null,
                false
            )
        }

        private fun createCommonDialog(
            context: Context,
            messageTitle: String?,
            messageContent: String?,
            strPositive: String?,
            strNegative: String?,
            strNeutral: String?,
            positiveClickListener: View.OnClickListener?,
            negativeClickListener: View.OnClickListener?,
            neutralClickListener: View.OnClickListener?,
            cancelable: Boolean
        ): Dialog {
            val alBuild = AlertDialog.Builder(context)
            alBuild.setCancelable(cancelable)

            val alertDialog = alBuild.create()
            alertDialog.show()

            val contentView = LayoutInflater.from(context).inflate(R.layout.common_dialog, null)
            alertDialog.setContentView(contentView)

            // Title
            if (null != messageTitle && !messageTitle.isEmpty()) {
                contentView.tvTitle.text = messageTitle
            } else {
                contentView.tvTitle.visibility = View.GONE
            }

            // Content
            if (null != messageContent && !messageContent.isEmpty()) {
                contentView.tvContent.text = messageContent
                contentView.tvContent.movementMethod = ScrollingMovementMethod()
            }

            // Button Neutual
            if (null != strNeutral && !strNeutral.isEmpty()) {
                contentView.btNeutral.visibility = View.VISIBLE
                contentView.btNeutral.text = strNeutral
                contentView.btNeutral.setOnClickListener {
                    neutralClickListener?.onClick(contentView)
                    alertDialog.dismiss()
                }
            } else {
                contentView.btNeutral.visibility = View.GONE
            }

            // Button Negative
            if (null != strNegative && !strNegative.isEmpty()) {
                contentView.btNo.visibility = View.VISIBLE
                contentView.btNo.text = strNegative
                contentView.btNo.setOnClickListener {
                    negativeClickListener?.onClick(contentView.btNo)
                    alertDialog.dismiss()
                }
            } else {
                contentView.btNo.visibility = View.GONE
            }

            // Button Positive
            if (null != strPositive && !strPositive.isEmpty()) {
                contentView.btYes.visibility = View.VISIBLE
                contentView.btYes.text = strPositive
                contentView.btYes.setOnClickListener {
                    positiveClickListener?.onClick(contentView.btYes)
                    alertDialog.dismiss()
                }
            } else {
                contentView.btYes.visibility = View.GONE
            }

            return alertDialog
        }

    }
}