package com.tamhuynh.gemalto

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import okhttp3.HttpUrl
import tamhuynh.gemalto.mylibrary.ServiceWrapper
import tamhuynh.gemalto.mylibrary.network.NetworkSettings

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        var networkSetting : NetworkSettings = NetworkSettings.Builder().
            url(HttpUrl.parse("https://randomuser.me/api/")).build()
        ServiceWrapper.init(this, networkSetting)
    }

    companion object {
        var isConnected = false
        var isShowDialog = 0


        @SuppressLint("StaticFieldLeak")
        @get:Synchronized
        lateinit var instance: MyApplication

        fun appContext(): Context? {
            return instance.applicationContext
        }
    }

}