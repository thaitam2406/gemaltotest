package com.tamhuynh.gemalto

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.tamhuynh.gemalto.interfaces.NavigateScreenEvent
import com.tamhuynh.gemalto.view.MainViewModel
import com.tamhuynh.gemalto.view.QueryUserFragment
import com.tamhuynh.gemalto.view.UserDetailsFragment
import com.tamhuynh.gemalto.view.MultipleUserFragment
import com.tamhuynh.gemalto.view.base.BaseFragmentActivityCompat
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.navigation_drawer_menu.*
import org.greenrobot.eventbus.Subscribe

class MainActivity : BaseFragmentActivityCompat(), View.OnClickListener {

    var tag = ""

    override fun isShowProgressBarInit() = false

    override fun getLayoutRedId(): Int {
        return R.layout.activity_main
    }

    override fun getFragmentContainerResId(): Int {
        return R.id.container_tab_home
    }

    override fun initData() {
    }

    override fun initUI() {
        toolbar.imgToggle.setOnClickListener(this)
        itemQuery.setOnClickListener(this)
        itemUsersStored.setOnClickListener(this)
        itemExit.setOnClickListener(this)
        buttonStoredAndDelete.setOnClickListener(this)
        showScreen(QueryUserFragment::class.java.name, Bundle())
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgToggle -> {
                if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.openDrawer(GravityCompat.START)
            }
            R.id.itemQuery -> {
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START)
                showScreen(QueryUserFragment::class.java.name, Bundle())
            }
            R.id.itemUsersStored -> {
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START)
//                mTitleMap[MultipleUserFragment::class.java.name] = getString(R.string.view_stored_user)
                showScreen(MultipleUserFragment::class.java.name + "_stored", Bundle())
            }

            R.id.itemExit -> {
                android.os.Process.killProcess(android.os.Process.myPid());
            }

            R.id.buttonStoredAndDelete ->{
                if(buttonStoredAndDelete.text.toString() == getString(R.string.stored_all)){
                    var fragment = supportFragmentManager.findFragmentByTag(MultipleUserFragment::class.java.name )
                    if(fragment != null && fragment is MultipleUserFragment) {
                        Toast.makeText(this@MainActivity, "Store", Toast.LENGTH_LONG).show()
                        fragment.storedMultipeUser()
                    }
                }else{
                    var fragment = supportFragmentManager.findFragmentByTag(MultipleUserFragment::class.java.name+ "_stored")
                    if(fragment != null && fragment is MultipleUserFragment) {
                        Toast.makeText(this@MainActivity, "Delete", Toast.LENGTH_LONG).show()
                        fragment.deleteAllUser()
                    }
                }
            }
        }
    }

    @Subscribe
    fun onNavigateScreenEvent(event: NavigateScreenEvent?) {
        val tagScreen: String? = event?.tagScreen
        tagScreen?.let {
            tag = it
        }
        val data: Bundle? = event?.bundleData!!
        if (tag != "" && data != null) {
            showScreen(tag, data)
        }
    }

    private fun showButtonActionBar(isStored : Boolean){
        buttonStoredAndDelete.text = if (isStored) getString(R.string.stored_all) else getString(R.string.delete_all)
        buttonStoredAndDelete.visibility = View.VISIBLE
        if(isStored){
            tvTitle.text = getString(R.string.query_multi_user)
        }else{
            tvTitle.text = getString(R.string.view_stored_user)
        }
    }

    private fun hideButtonActionBar(){
        buttonStoredAndDelete.visibility = View.GONE
    }

    override fun initNavigationFragment() {
        NAVIGABLE_FRAGMENTS.add(QueryUserFragment::class.java.name)
        NAVIGABLE_FRAGMENTS.add(MultipleUserFragment::class.java.name)
        NAVIGABLE_FRAGMENTS.add(UserDetailsFragment::class.java.name)
    }

    override fun initTitleToolBar(title: String?) {
        mTitleMap[QueryUserFragment::class.java.name] = getString(R.string.query_user)
        mTitleMap[UserDetailsFragment::class.java.name] = getString(R.string.query_user)
//        mTitleMap[MultipleUserFragment::class.java.name + "_stored"] = getString(R.string.view_stored_user)
    }

    override fun updateToolbar(currentFragment: String?, s: String?) {
        if (null != s)
            tvTitle.text = s
        when {
            currentFragment.equals(MultipleUserFragment::class.java.name + "_stored") -> showButtonActionBar(false)
            currentFragment.equals(MultipleUserFragment::class.java.name ) -> showButtonActionBar(true)
            else -> hideButtonActionBar()
        }
    }

    override fun initLogicBackground() {
    }

/*    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    // Example of a call to a native method
        sample_text.text = stringFromJNI("a")
//        var securityHelp : SecurityHelp = SecurityHelp()
//        securityHelp.decypText(convertStringToHex("test"))

    }*/

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(a: String): String

    companion object {

        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
    }

    fun convertStringToHex(s: String?): String? {
        if (s == null)
            return null
        if (s.length <= 0)
            return ""
        val hex = StringBuilder()
        for (i in 0 until s.length) {
            hex.append(Integer.toHexString(s[i].toInt()))
        }
        return hex.toString()
    }
}
