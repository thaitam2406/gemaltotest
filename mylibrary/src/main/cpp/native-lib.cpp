#include <jni.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <openssl/aes.h>
#include <android/log.h>
#include <string>
#include <stdexcept>

using namespace std;

unsigned char key[32] = {19, 10, 128, 17, 9, 6, 80, 9, 101, 3, 4, 6, 7, 8, 99, 2, 19, 10, 88, 17, 9, 176, 8, 90, 10, 3, 4, 6, 7, 8, 99, 200};
unsigned char iv[32] = {1, 3, 115, 6, 7, 80, 9, 0, 2, 4, 50, 7, 101, 44, 66, 77,19, 10, 8, 17, 9, 6, 8, 99, 1, 3, 214, 6, 7, 8, 99, 2};

static void copyArr(const unsigned char *arrSource,unsigned char *arrayDes,size_t size)
{
    for (int i = 0; i < size; i++)
    {
        arrayDes[i] = arrSource[i];
    }
}

static int char2int(char input)
{
    if(input >= '0' && input <= '9')
        return input - '0';
    if(input >= 'A' && input <= 'F')
        return input - 'A' + 10;
    if(input >= 'a' && input <= 'f')
        return input - 'a' + 10;
    throw std::invalid_argument("Invalid input string");
}
static void hex2bin(const char* src, char* target)
{
    while(*src && src[1])
    {
        *(target++) = char2int(*src)*16 + char2int(src[1]);
        src += 2;
    }
}

static void dec_aes(unsigned char *stringDesInput,unsigned char * dec_out, long inputLeng)
{
    AES_KEY dec_key;
    unsigned char *copyIV = ( unsigned char *)malloc(32);
    unsigned char *copyKey = ( unsigned char *)malloc(32);
    copyArr(iv,copyIV,32);
    copyArr(key,copyKey,32);

    unsigned char *aes_input = (unsigned char *)stringDesInput;
    const size_t encslength = ((inputLeng + AES_BLOCK_SIZE) / AES_BLOCK_SIZE) * AES_BLOCK_SIZE;
    AES_set_decrypt_key(copyKey, 256, &dec_key);
    AES_cbc_encrypt(aes_input, dec_out, encslength, &dec_key, copyIV, AES_DECRYPT);
    free(copyIV);
    free(copyKey);
}


static void enc_aes(unsigned char *stringEncInput, unsigned char * dec_out, long inputLeng)
{
    AES_KEY dec_key;
    unsigned char *copyIV = ( unsigned char *)malloc(32);
    unsigned char *copyKey = ( unsigned char *)malloc(32);
    copyArr(iv,copyIV,32);
    copyArr(key,copyKey,32);

    unsigned char *aes_input = (unsigned char *)stringEncInput;
    const size_t encslength = ((inputLeng + AES_BLOCK_SIZE) / AES_BLOCK_SIZE) * AES_BLOCK_SIZE;
    AES_set_encrypt_key(copyKey, 256, &dec_key);
    AES_cbc_encrypt(aes_input, dec_out, encslength, &dec_key, copyIV, AES_ENCRYPT);
    free(copyIV);
    free(copyKey);
}

//end@@end
/*static void truncat(char *data)
{
    size_t len = strlen(data) - 1;
    for(size_t i = len ; i >= 0; i-- )
    {
        if (data[i] == 'd' && data[i-1] == 'n' && data[i-2] == 'e'
            && data[i-3] == '@' && data[i-4] == '@' && data[i-5] == 'd' && data[i-6] == 'n' && data[i-7] == 'e')
        {

            data[i] = '\0';
            data[i-1] = '\0';
            data[i-2] = '\0';
            data[i-3] = '\0';
            data[i-4] = '\0';
            data[i-5] = '\0';
            data[i-6] = '\0';
            data[i-7] = '\0';
            break;

        }
        data[i]= '\0';

    }

}*/

//end@end
static void truncatFile(unsigned char *data,long len)
{
    for(long i = len ; i >= 0; i-- )
    {
        if (data[i] == 'd' && data[i-1] == 'n' && data[i-2] == 'e'
            && data[i-3] == '@' && data[i-4] == 'd' && data[i-5] == 'n' && data[i-6] == 'e')
        {

            data[i] = '\0';
            data[i-1] = '\0';
            data[i-2] = '\0';
            data[i-3] = '\0';
            data[i-4] = '\0';
            data[i-5] = '\0';
            data[i-6] = '\0';
            break;

        }
        data[i]= '\0';

    }

}

static unsigned char *dec_aes_byte(unsigned char *stringDesInput, long inputLeng)
{
    AES_KEY dec_key;
    unsigned char *copyIV = ( unsigned char *)malloc(32);
    unsigned char *copyKey = ( unsigned char *)malloc(32);
    copyArr(iv,copyIV,32);
    copyArr(key,copyKey,32);

    unsigned char *aes_input = stringDesInput;
    const size_t encslength = ((inputLeng + AES_BLOCK_SIZE) / AES_BLOCK_SIZE) * AES_BLOCK_SIZE;
    unsigned char *dec_out = (unsigned char*)malloc(sizeof(unsigned char) *inputLeng);

    AES_set_decrypt_key(copyKey, 256, &dec_key);
    AES_cbc_encrypt(aes_input, dec_out, encslength, &dec_key, copyIV, AES_DECRYPT);

    free(copyIV);
    free(copyKey);

    return dec_out;
}

static unsigned char *readfile(const char *fileName, long *outSize){
    FILE * file = fopen(fileName, "rb");
    if (file == NULL) {
        printf("File null");
        return NULL;
    }
    fseek(file, 0, SEEK_END);
    long size = ftell(file);
    *outSize = size;
    rewind(file);
    unsigned char * in = (unsigned char *) malloc(size);
    int bytes_read = fread(in, sizeof(unsigned char), size, file);
    fclose(file);
    return in;
}

extern "C" JNIEXPORT jstring JNICALL Java_tamhuynh_gemalto_mylibrary_SecurityHelp_decrypt(
        JNIEnv *env, jobject /* this */,jstring hexString)
{

    __android_log_print(ANDROID_LOG_ERROR, "TRACKERS", "start");
    const char *arrCharHex = env->GetStringUTFChars(hexString, 0);
    std::string dataEnc(arrCharHex);


    size_t lengString = dataEnc.size();
    size_t sizeLong = 16;// sizeof(long) * 2;
    std::string truthSize = dataEnc.substr(lengString - sizeLong, lengString);
    std::string truthEnc = dataEnc.substr(0, lengString - sizeLong);

//    printf("sub0:%d\n",sizeLong);
//    printf("sub1:%s\n",truthSize.c_str());
//    printf("sub2:%s\n",truthEnc.c_str());


    char *dataEncStr = (char *) malloc(sizeof(unsigned char) * truthEnc.size() / 2);
    char *dataLeng = (char *) malloc(sizeLong / 2);
//    int tempResult =
    hex2bin(truthEnc.c_str(), dataEncStr);
//    if(tempResult<0){
//        env->ReleaseStringUTFChars(hexString, arrCharHex);
//        free(dataEncStr);
//        free(dataLeng);
//        jstring result = env->NewStringUTF("NULL");
//        return result;
//    }

//    tempResult =
    hex2bin(truthSize.c_str(), dataLeng);
//    if(tempResult<0){
//        env->ReleaseStringUTFChars(hexString, arrCharHex);
//        free(dataEncStr);
//        free(dataLeng);
//        jstring result = env->NewStringUTF("NULL");
//        return result;
//    }

    long *value = (long *) dataLeng;
//    printf("truevalue:%lu\n",*value);

    unsigned char *dec_out = (unsigned char *) malloc(sizeof(unsigned char) * (*value));

    unsigned char *enc_out = (unsigned char *) malloc(sizeof(unsigned char) * (*value));
    enc_aes(dec_out, enc_out, *value);
    __android_log_print(ANDROID_LOG_VERBOSE, "TRACKERS", "encrypt :%s", dec_out);

    dec_aes(enc_out, dec_out, *value);
    __android_log_print(ANDROID_LOG_VERBOSE, "TRACKERS", "decrypt :%s", dec_out);


//    truncat((char *) dec_out);
    jstring result = env->NewStringUTF((char *) dec_out);
    env->ReleaseStringUTFChars(hexString, arrCharHex);
    free(dataEncStr);
    free(dataLeng);
    free(dec_out);
    return result;
}

/*
extern "C" JNIEXPORT jstring JNICALL Java_com_lazyduck_story_native_NativeC_decryptFile(
        JNIEnv *env, jobject,jstring fileName)
{
    const char *fileStr = env->GetStringUTFChars(fileName, 0);
    long *size = (long *)malloc(8);//sizeof(long));
    unsigned char *encStr = readfile(fileStr,size);

    if(encStr == NULL){
        jstring result = env->NewStringUTF("NULL") ;
        env->ReleaseStringUTFChars(fileName, fileStr);
        return result;
    }

    unsigned char *lengByte = encStr + *size - 8;//sizeof(long);
    long *lengLong  = (long *)lengByte;

    unsigned char * dataToDecryt = (unsigned char *)malloc(*size - 8);//sizeof(long));
    memcpy(dataToDecryt,encStr,*size - 8);//sizeof(long));
    unsigned char *dataPure =  dec_aes_byte(dataToDecryt,*lengLong);

    truncatFile(dataPure,*lengLong);
    jstring result = env->NewStringUTF((char *) dataPure);


    free(size);
    free(encStr);
    free(dataPure);
    free(dataToDecryt);
    env->ReleaseStringUTFChars(fileName, fileStr);

    return result;
}*/
