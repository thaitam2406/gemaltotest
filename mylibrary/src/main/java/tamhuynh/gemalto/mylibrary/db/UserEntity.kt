package tamhuynh.gemalto.mylibrary.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "users")
data class UserEntity(@PrimaryKey
                @ColumnInfo(name = "id")
                val id: String = UUID.randomUUID().toString(),
                @ColumnInfo(name = "username")
                val userName: String,
                @ColumnInfo(name = "gender")
                val gender: String,
                @ColumnInfo(name = "age")
                val age: String,
                @ColumnInfo(name = "dob")
                val dob: String,
                @ColumnInfo(name = "email")
                val email: String,
                @ColumnInfo(name = "seek")
                val seek: String
)