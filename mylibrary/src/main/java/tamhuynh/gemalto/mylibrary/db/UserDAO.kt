package tamhuynh.gemalto.mylibrary.db

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Flowable
import java.util.ArrayList

@Dao
interface UserDAO {

    @Query("SELECT * FROM USERS")
    fun getAllUsers() : Flowable<List<UserEntity>>

    @Query("SELECT * FROM USERS WHERE gender = :gender")
    fun getUserViaGender(gender: String) : Flowable<UserEntity>

    @Query("SELECT * FROM USERS WHERE seek = :seek")
    fun getUserViaSeed(seek: String) : Flowable<UserEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun inserUser(userEntity : UserEntity) : Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMultipleUser(listUserEntity : List<UserEntity>) : Completable

    @Query("DELETE FROM USERS WHERE username = :userId")
    fun deleteUser(userId : String) : Completable

    @Query("DELETE FROM USERS")
    fun deleteAllUser() : Completable
}