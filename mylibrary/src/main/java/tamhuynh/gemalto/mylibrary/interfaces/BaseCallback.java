package tamhuynh.gemalto.mylibrary.interfaces;


public interface BaseCallback<T> {
    void onSuccess(T t);

    void onError(Throwable e);
}
