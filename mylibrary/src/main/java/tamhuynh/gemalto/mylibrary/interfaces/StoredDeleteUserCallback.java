package tamhuynh.gemalto.mylibrary.interfaces;

/**
 * Created by TamHuynh on 14,March,2019
 */
public interface StoredDeleteUserCallback {

    void onSuccess();

    void onFail();
}
