package tamhuynh.gemalto.mylibrary.interfaces;

import java.util.ArrayList;
import java.util.List;

import tamhuynh.gemalto.mylibrary.db.UserEntity;

/**
 * Created by TamHuynh on 13,March,2019
 */
public interface GetUserStoredCallBack {

    void onListUser(List<UserEntity> userEntity);

    void onError();
}
