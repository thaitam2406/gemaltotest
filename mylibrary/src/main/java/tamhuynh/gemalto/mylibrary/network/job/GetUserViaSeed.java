package tamhuynh.gemalto.mylibrary.network.job;

import com.birbit.android.jobqueue.Params;
import retrofit2.Response;
import tamhuynh.gemalto.mylibrary.network.model.BaseResponse;
import tamhuynh.gemalto.mylibrary.network.model.user.UserModel;

public class GetUserViaSeed extends SpSessionJob<UserModel> {

    private final String mSeed;

    public GetUserViaSeed(String gender) {
        super(new Params(10)
                .singleInstanceBy("GetUserViaSeed"), UserModel.class);
        this.mSeed = gender;
    }


    @Override
    protected void onConstructParams() {
        map.put("seed", mSeed);
    }

    @Override
    Response<BaseResponse<UserModel>> onSynchronousRun() throws Exception {
        return getApiService().getUserViaSeed( map).execute();
    }
}
