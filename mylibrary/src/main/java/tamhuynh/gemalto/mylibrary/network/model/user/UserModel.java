
package tamhuynh.gemalto.mylibrary.network.model.user;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import tamhuynh.gemalto.mylibrary.network.model.Info;

public class UserModel {

    @SerializedName("results")
    @Expose
    private List<Results> results = null;
    @SerializedName("info")
    @Expose
    private Info info;

    public List<Results> getResults() {
        return results;
    }

    public void setResults(List<Results> results) {
        this.results = results;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public String getUserID(int index){
        return  results.get(index).getId().getValue() == null ? getUserEmail(index) : results.get(index).getId().getValue();
    }

    public String getUserName(int index){
        return  results.get(index).getName().getFirst() + results.get(index).getName().getLast();
    }

    public String getUsergender(int index){
        return  results.get(index).getGender();
    }

    public String getUserAge(int index){
        return  results.get(index).getDob().getAge().toString();
    }

    public String getUserDOB(int index){
        return  results.get(index).getDob().getDate();
    }

    public String getUserEmail(int index){
        return  results.get(index).getEmail();
    }

    public String getSeed(int index){
        return results.get(index).getInfo().getSeed();
    }

}
