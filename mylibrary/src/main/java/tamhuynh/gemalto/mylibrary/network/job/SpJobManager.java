package tamhuynh.gemalto.mylibrary.network.job;

import android.content.Context;
import android.os.Build;
import androidx.annotation.VisibleForTesting;
import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.birbit.android.jobqueue.scheduling.FrameworkJobSchedulerService;
import com.birbit.android.jobqueue.scheduling.GcmJobSchedulerService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import io.reactivex.Single;
import io.reactivex.subjects.SingleSubject;
import tamhuynh.gemalto.mylibrary.network.*;
import timber.log.Timber;

import java.util.Hashtable;

public class SpJobManager {

    private static SpJobManager sInstance;
    private final Context mContext;
    private JobManager mJobManager;
    private Hashtable<String, SingleSubject> mSubjects = new Hashtable<>();

    @VisibleForTesting
    protected SpJobManager(Context context) {
        mContext = context;
    }

    public static SpJobManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SpJobManager(context);
        }
        return sInstance;
    }

    public synchronized JobManager getJobManager() {
        if (mJobManager == null) {
            configure();
        }
        return mJobManager;
    }

    private synchronized void configure() {
        Timber.d("JobManager::configure()");
        Configuration.Builder builder = new Configuration.Builder(mContext)
                .minConsumerCount(0)
                .maxConsumerCount(5)
                .loadFactor(1)
                .injector(new DependencyInjector(this))
                .consumerKeepAlive(30);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.scheduler(FrameworkJobSchedulerService.createSchedulerFor(mContext,
                    JobService.class), false);
        } else {
            int enableGcm = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(mContext);
            if (enableGcm == ConnectionResult.SUCCESS) {
                builder.scheduler(GcmJobSchedulerService.createSchedulerFor(mContext,
                        GcmJobService.class), false);
            }
        }
        mJobManager = new JobManager(builder.build());
    }

    protected ApiService getApiService() {
        return NetworkModule.getInstance(mContext).getApiService();
    }

    public void queue(SpJob job) {
        if (!mSubjects.containsKey(job.subjectId)) {
            Timber.w("Queueing without listener %1$s", job.subjectId);
        }
        getJobManager().addJobInBackground(job);
    }

    public synchronized <T> Single<T> getListener(SpJob<T> job) {
        String id = job.subjectId;
        SingleSubject<T> subject;
        if (mSubjects.containsKey(id)) {
            //noinspection unchecked
            subject = mSubjects.get(id);
        } else {
            Timber.d("Creating new Subject for subjectId %1$s", id);
            subject = SingleSubject.create();
            mSubjects.put(id, subject);
        }
        return subject;
    }

    public synchronized <T> void onSuccess(SpJob<T> job, T value) {
        String id = job.subjectId;
        Timber.d("Success of job with subjectId %1$s", id);
        if (mSubjects.containsKey(id)) {
            SingleSubject subject = mSubjects.get(id);
            mSubjects.remove(id);
            if (subject.hasObservers()) {
                //noinspection unchecked
                subject.onSuccess(value);
            } else {
                Timber.w("Result for job id: %1$s is being ignored because there's no observer!", id);
            }
        }
    }

    public synchronized <T> void onError(SpJob<T> job, Throwable throwable) {
        String id = job.subjectId;
        Timber.d("Error of job with subjectId %1$s", id);
        if (mSubjects.containsKey(id)) {
            SingleSubject subject = mSubjects.get(id);
            mSubjects.remove(id);
            if (subject.hasObservers()) {
                subject.onError(throwable);
            } else {
                Timber.w("Error for job id: %1$s is being ignored because there's no observer!", id);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpJobManager that = (SpJobManager) o;

        return mSubjects != null ? mSubjects.equals(that.mSubjects) : that.mSubjects == null;
    }

    @Override
    public int hashCode() {
        return mSubjects != null ? mSubjects.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "JobManager{" +
                ", mJobManager=" + mJobManager +
                ", mSubjects=" + mSubjects.size() +
                '}';
    }
}
