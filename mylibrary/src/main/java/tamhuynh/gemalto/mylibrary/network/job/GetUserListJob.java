package tamhuynh.gemalto.mylibrary.network.job;

import com.birbit.android.jobqueue.Params;
import retrofit2.Response;
import tamhuynh.gemalto.mylibrary.network.model.BaseResponse;
import tamhuynh.gemalto.mylibrary.network.model.user.UserModel;

public class GetUserListJob extends SpSessionJob<UserModel> {

    private final int mCount;

    public GetUserListJob(int count) {
        super(new Params(10)
                .singleInstanceBy("GetUserListJob"), UserModel.class);
        this.mCount = count;
    }


    @Override
    protected void onConstructParams() {
        map.put("results", mCount);
    }

    @Override
    Response<BaseResponse<UserModel>> onSynchronousRun() throws Exception {
        return getApiService().getUserList( map).execute();
    }
}
