package tamhuynh.gemalto.mylibrary.network;

import androidx.annotation.NonNull;
import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.scheduling.GcmJobSchedulerService;
import tamhuynh.gemalto.mylibrary.network.job.SpJobManager;

public class GcmJobService extends GcmJobSchedulerService {

    @NonNull
    @Override
    protected JobManager getJobManager() {
        return SpJobManager.getInstance(getApplicationContext()).getJobManager();
    }
}
