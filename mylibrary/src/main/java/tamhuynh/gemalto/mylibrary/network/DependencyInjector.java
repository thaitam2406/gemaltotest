package tamhuynh.gemalto.mylibrary.network;

import com.birbit.android.jobqueue.Job;
import tamhuynh.gemalto.mylibrary.network.job.SpJob;
import tamhuynh.gemalto.mylibrary.network.job.SpJobManager;

public class DependencyInjector implements com.birbit.android.jobqueue.di.DependencyInjector {
    private final SpJobManager mSpJobManager;

    public DependencyInjector(SpJobManager spJobManager) {
        mSpJobManager = spJobManager;
    }

    @Override
    public void inject(Job job) {
        if (job instanceof SpJob) {
            ((SpJob) job).setSpJobManager(mSpJobManager);
        }
    }
}
