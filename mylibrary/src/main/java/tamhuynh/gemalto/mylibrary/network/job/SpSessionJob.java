package tamhuynh.gemalto.mylibrary.network.job;

import androidx.annotation.NonNull;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

abstract class SpSessionJob<T> extends SpJob<T> {

    SpSessionJob(Params params, Class<? super T> type) {
        super(params, type);
    }

    abstract protected void onConstructParams();

    @Override
    public void onRun() throws Throwable {
        onConstructParams();
        super.onRun();

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return super.shouldReRunOnThrowable(throwable, runCount, maxRunCount);
    }
}
