package tamhuynh.gemalto.mylibrary.network;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import okhttp3.OkHttpClient;
import timber.log.Timber;

import java.util.concurrent.TimeUnit;

public class NetworkComponent {

    private static NetworkComponent sInstance;

    private static final int READ_TIMEOUT_MIN = 2;
    private static final int CONNECT_TIMEOUT_SECONDS = 10;

    private NetworkSettings defaultSettings;

    public static NetworkComponent getInstance() {
        if (sInstance == null) {
            sInstance = new NetworkComponent();
        }
        return sInstance;
    }

    private NetworkComponent() {
    }

    public OkHttpClient.Builder clientDefaultBuilder() {
        return new OkHttpClient.Builder()
                .readTimeout(READ_TIMEOUT_MIN, TimeUnit.MINUTES)
                .connectTimeout(CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @Nullable
    public NetworkSettings getDefaultSettings() {
        return defaultSettings;
    }

    public void setDefaultSettings(@NonNull NetworkSettings defaultSettings) {
        if (this.defaultSettings != null) {
            Timber.e("Trying to set default settings twice!");
            throw new IllegalArgumentException("Can only set default settings once.");
        }
        this.defaultSettings = defaultSettings;
    }
}
