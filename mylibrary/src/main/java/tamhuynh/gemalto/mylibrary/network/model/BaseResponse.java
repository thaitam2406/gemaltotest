package tamhuynh.gemalto.mylibrary.network.model;

import com.google.gson.annotations.SerializedName;

public class BaseResponse<T> {
    @SerializedName("result")
    private T mResult;

    public T getResult() {
        return mResult;
    }

}
