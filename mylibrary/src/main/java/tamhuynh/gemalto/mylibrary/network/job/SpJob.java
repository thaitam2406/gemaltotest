package tamhuynh.gemalto.mylibrary.network.job;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.birbit.android.jobqueue.CancelReason;
import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import retrofit2.Response;
import tamhuynh.gemalto.mylibrary.network.ApiService;
import tamhuynh.gemalto.mylibrary.network.model.BaseResponse;
import timber.log.Timber;

import java.lang.ref.WeakReference;
import java.util.LinkedHashMap;
import java.util.concurrent.TimeoutException;

public abstract class SpJob<T> extends Job {
    private static final int DEFAULT_DEADLINE_IN_MS = 30000;
    private static final int DEFAULT_RETRY_LIMIT = 3;
    protected final LinkedHashMap<String, Object> map;
    protected final String subjectId;
    //Mark as transient to prevent it from being serialized.
    private transient WeakReference<SpJobManager> mSpJobManagerRef;

    abstract Response<BaseResponse<T>> onSynchronousRun() throws Exception;

    SpJob(Params params, Class<? super T> type) {
        this(params, DEFAULT_DEADLINE_IN_MS, type);
    }

    SpJob(Params params, long deadlineInMs, Class<? super T> type) {
        super(params
                .requireNetwork()
                .overrideDeadlineToCancelInMs(deadlineInMs));
        subjectId = getSingleInstanceId() + "." + type.getName();
        map = new LinkedHashMap<>();
    }

    @Override
    protected int getRetryLimit() {
        return DEFAULT_RETRY_LIMIT;
    }

    public void setSpJobManager(SpJobManager spJobManager) {
        mSpJobManagerRef = new WeakReference<>(spJobManager);
    }

    protected ApiService getApiService() {
        return getSpJobManager().getApiService();
    }

    protected SpJobManager getSpJobManager() {
        if (mSpJobManagerRef.get() == null) {
            Timber.w("Null Job Manager, failed to inject.");
            mSpJobManagerRef = new WeakReference<>(SpJobManager.getInstance(getApplicationContext()));
        }
        return mSpJobManagerRef.get();
    }

    @Override
    public void onAdded() {
        Timber.d("Job %1$s Added to disk.", subjectId);
    }

    @Override
    public void onRun() throws Throwable {
        Timber.d("Running job %1$s", subjectId);
        try {
            Response<? extends BaseResponse<T>> response = onSynchronousRun();
            if (response == null) {
                throw new RuntimeException("Response Error");
            }
            if (response.isSuccessful()) {
                BaseResponse<T> body = response.body();
                T result = body.getResult();
                result = onValidateResult(result);
                if (result == null) {
                    throw new RuntimeException("Response Error");
                }
                onSuccess(result);
            } else {
                throw new RuntimeException("Response Error");
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    protected T onValidateResult(T t) throws RuntimeException {
        return t;
    }

    @Override
    protected void onCancel(@CancelReason int cancelReason, @Nullable Throwable throwable) {
        if (throwable == null || throwable instanceof TimeoutException) {
            //Reached deadline, throwable is null or is a timeout.
            throwable = new RuntimeException("Network Error", throwable);
        }
        Timber.e(throwable, "Job %1$s is canceled. Reason: %2$d", subjectId, cancelReason);
        switch (cancelReason) {
            case CancelReason.CANCELLED_VIA_SHOULD_RE_RUN:
            case CancelReason.CANCELLED_WHILE_RUNNING:
            case CancelReason.REACHED_DEADLINE:
            case CancelReason.REACHED_RETRY_LIMIT:
                onError(throwable);
                break;
            case CancelReason.SINGLE_INSTANCE_ID_QUEUED:
            case CancelReason.SINGLE_INSTANCE_WHILE_RUNNING:
                // Ignore these 2.
                break;
        }
    }

    protected void onSuccess(T result) {
        getSpJobManager().onSuccess(this, result);
    }

    private void onError(@Nullable Throwable throwable) {
        getSpJobManager().onError(this, throwable);
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        Timber.d("Job should rerun? %1$s count: %2$d", throwable.getClass().getName(), runCount);
        if (throwable.getMessage().equalsIgnoreCase("Response Error")) {
            Timber.i("Cancelling job because of: %1$s", throwable.getMessage());
            return RetryConstraint.CANCEL;
        }
        RetryConstraint backoff = RetryConstraint.createExponentialBackoff(runCount, 1000);
        Timber.d("Backoff for: %1$d ms", backoff.getNewDelayInMs());
        return backoff;
    }

}
