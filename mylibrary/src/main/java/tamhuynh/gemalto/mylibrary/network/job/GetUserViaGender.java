package tamhuynh.gemalto.mylibrary.network.job;

import com.birbit.android.jobqueue.Params;
import retrofit2.Response;
import tamhuynh.gemalto.mylibrary.network.model.BaseResponse;
import tamhuynh.gemalto.mylibrary.network.model.user.UserModel;

public class GetUserViaGender extends SpSessionJob<UserModel> {

    private final String mGender;

    public GetUserViaGender(String gender) {
        super(new Params(10)
                .singleInstanceBy("GetUserViaGender"), UserModel.class);
        this.mGender = gender;
    }


    @Override
    protected void onConstructParams() {
        map.put("gender", mGender);
    }

    @Override
    Response<BaseResponse<UserModel>> onSynchronousRun() throws Exception {
        return getApiService().getUserViaGender( map).execute();
    }
}
