package tamhuynh.gemalto.mylibrary.network.job;

import tamhuynh.gemalto.mylibrary.interfaces.BaseCallback;
import tamhuynh.gemalto.mylibrary.network.model.user.UserModel;

public interface GetUserCalback extends BaseCallback<UserModel> {
}
