package tamhuynh.gemalto.mylibrary.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import tamhuynh.gemalto.mylibrary.network.model.BaseResponse;
import tamhuynh.gemalto.mylibrary.network.model.user.UserModel;

import java.util.Map;

public interface ApiService {


    @GET(".")
    Call<BaseResponse<UserModel>> getUserList(@QueryMap Map<String, Object> queries);

    @GET(".")
    Call<BaseResponse<UserModel>> getUserViaGender(@QueryMap Map<String, Object> queries);

    @GET(".")
    Call<BaseResponse<UserModel>> getUserViaSeed(@QueryMap Map<String, Object> queries);

}
