package tamhuynh.gemalto.mylibrary.network;

import okhttp3.HttpUrl;
import timber.log.Timber;

public class NetworkSettings {

    public final HttpUrl url;

    private NetworkSettings(HttpUrl url) {
        this.url = url;
    }

    public static class Builder {
        private HttpUrl mUrl;

        public Builder url(HttpUrl url) {
            mUrl = url;
            return this;
        }


        public NetworkSettings build() {
            if (mUrl == null) {
                Timber.e("Url is null!");
                throw new IllegalArgumentException("Url cannot be null.");
            }
            return new NetworkSettings(mUrl);
        }
    }
}
