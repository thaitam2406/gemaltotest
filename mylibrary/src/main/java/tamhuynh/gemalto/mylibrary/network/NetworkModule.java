package tamhuynh.gemalto.mylibrary.network;

import android.content.Context;
import androidx.annotation.NonNull;
import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tamhuynh.gemalto.mylibrary.ServiceWrapper;
import tamhuynh.gemalto.mylibrary.ServiceWrapperImpl;
import tamhuynh.gemalto.mylibrary.network.job.SpJobManager;
import timber.log.Timber;

import java.io.IOException;

public class NetworkModule {

    private static NetworkModule sNetworkModule;
    private static NetworkSettings sNetworkSettings;
    protected final NetworkSettings mNetworkSettings;

    private Context mAppContext;
    private ServiceWrapper mServiceWrapper;
    private ApiService mApiService;

    private NetworkModule(Context appContext, NetworkSettings sNetworkSettings) {
        mAppContext = appContext;
        mNetworkSettings = sNetworkSettings;
    }

    public static NetworkModule getInstance(Context appContext) {
        if (sNetworkModule == null) {
            if (sNetworkSettings == null) {
                sNetworkSettings = NetworkComponent.getInstance().getDefaultSettings();
            }
            sNetworkModule = new NetworkModule(appContext, sNetworkSettings);
        }
        return sNetworkModule;
    }

    public static void init(NetworkSettings networkSettings) {
        sNetworkSettings = networkSettings;
        sNetworkModule = null;
    }

    public HttpUrl getEndPoint() {
        return mNetworkSettings.url;
    }

    public ServiceWrapper getServiceWrapper() {
        if (mServiceWrapper == null) {
            mServiceWrapper = new ServiceWrapperImpl(mAppContext, SpJobManager.getInstance(mAppContext));
        }
        return mServiceWrapper;
    }

    public ApiService getApiService() {
        if (mApiService == null) {
            OkHttpClient okHttpClient = okHttpClient();
            HttpUrl httpUrl = getEndPoint();
            Retrofit retrofit = retrofit(okHttpClient, httpUrl);
            mApiService = retrofit.create(ApiService.class);
        }
        return mApiService;
    }

    private OkHttpClient okHttpClient() {

        HttpLoggingInterceptor loggingInterceptor = loggingInterceptor();

        NetworkComponent component = NetworkComponent.getInstance();
        OkHttpClient.Builder builder = component
                .clientDefaultBuilder()
                .addInterceptor(responseInterceptor())
                .addInterceptor(loggingInterceptor);

        return builder.build();
    }

    private Retrofit retrofit(OkHttpClient client, HttpUrl httpUrl) {
        return new Retrofit.Builder()
                .baseUrl(httpUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @NonNull
    private HttpLoggingInterceptor loggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(message -> Timber.d(message));
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return loggingInterceptor;
    }
    private Interceptor responseInterceptor(){
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Response response = chain.proceed(request);
                if (response.code() == 200) {
                    final ResponseBody body = response.body();
                    String stringBody = body.string();
                    String modifyBody = "{ \"result\":" + stringBody + "}";
                    ResponseBody newBody = ResponseBody.create(body.contentType(), modifyBody.getBytes());
                    return response.newBuilder()
                            .body(newBody)
                            .build();
                }
                return response;
            }
        };
    }
}
