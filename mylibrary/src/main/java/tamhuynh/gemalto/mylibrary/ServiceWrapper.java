package tamhuynh.gemalto.mylibrary;

import android.content.Context;

import java.util.List;

import tamhuynh.gemalto.mylibrary.db.UserEntity;
import tamhuynh.gemalto.mylibrary.interfaces.GetUserStoredCallBack;
import tamhuynh.gemalto.mylibrary.interfaces.StoredDeleteUserCallback;
import tamhuynh.gemalto.mylibrary.network.NetworkModule;
import tamhuynh.gemalto.mylibrary.network.NetworkSettings;
import tamhuynh.gemalto.mylibrary.network.job.GetUserCalback;
import timber.log.Timber;

public abstract class ServiceWrapper {

    private static ServiceWrapper serviceWrapper;

    public static void init(Context context, NetworkSettings networkSettings){
        serviceWrapper = null;
        NetworkModule.init(networkSettings);
        Timber.plant(new Timber.DebugTree());
    }

    public abstract void getListUser(int count, GetUserCalback callBack);

    public abstract void getListUserStored(GetUserStoredCallBack callBack);

    public abstract void getUserViaGender(String gender, GetUserCalback callBack);

    public abstract void storedUser(UserEntity userEntity, StoredDeleteUserCallback callBack);

    public abstract void storedMultipleUser(List<UserEntity> userEntity, StoredDeleteUserCallback callBack);

    public abstract void getUserViaSeed(String seed, GetUserCalback callBack);

    public abstract void deleteUser(UserEntity userEntity, StoredDeleteUserCallback callBack);

    public abstract void deleteAllUser(StoredDeleteUserCallback callBack);

}
