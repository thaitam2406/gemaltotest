package tamhuynh.gemalto.mylibrary;

public class SecurityHelp {

    static {
        // Used to load the 'security' library on application startup.
        System.loadLibrary("security");
    }

    public native String decrypt(String input);

    public void decypText(String str){
        decrypt(str);
    }
}
