package tamhuynh.gemalto.mylibrary;

import android.content.Context;

import java.util.List;

import androidx.annotation.Nullable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;
import io.reactivex.subscribers.ResourceSubscriber;
import tamhuynh.gemalto.mylibrary.db.UserEntity;
import tamhuynh.gemalto.mylibrary.db.UsersDatabase;
import tamhuynh.gemalto.mylibrary.interfaces.BaseCallback;
import tamhuynh.gemalto.mylibrary.interfaces.GetUserStoredCallBack;
import tamhuynh.gemalto.mylibrary.interfaces.StoredDeleteUserCallback;
import tamhuynh.gemalto.mylibrary.network.job.GetUserCalback;
import tamhuynh.gemalto.mylibrary.network.job.GetUserListJob;
import tamhuynh.gemalto.mylibrary.network.job.GetUserViaGender;
import tamhuynh.gemalto.mylibrary.network.job.GetUserViaSeed;
import tamhuynh.gemalto.mylibrary.network.job.SpJob;
import tamhuynh.gemalto.mylibrary.network.job.SpJobManager;
import tamhuynh.gemalto.mylibrary.network.model.user.UserModel;
import timber.log.Timber;

public class ServiceWrapperImpl extends ServiceWrapper {

    Context mContext;
    SpJobManager mSpJobManager;
    UsersDatabase usersDatabase;
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    public ServiceWrapperImpl(Context mAppContext, SpJobManager instance) {
        mContext = mAppContext;
        mSpJobManager = instance;
        usersDatabase = UsersDatabase.Companion.getInstance(mAppContext);
    }

    protected <T, S> Disposable subscribeAndQueue(Single<S> single, SpJob<T> job, @Nullable final BaseCallback<S> callback) {
        DisposableSingleObserver<S> disposable = single
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<S>() {
                    @Override
                    public void onSuccess(@io.reactivex.annotations.NonNull S s) {
                        if (callback != null) {
                            callback.onSuccess(s);
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        if (callback != null) {
                                callback.onError(e);
                        }
                    }
                });
        mSpJobManager.queue(job);
        return disposable;
    }

    @Override
    public void getListUser(int count, GetUserCalback callback) {
        Timber.d("getListUser");
        GetUserListJob job = new GetUserListJob(count);
        Single<UserModel> single = mSpJobManager.getListener(job);
        subscribeAndQueue(single, job, callback);
    }

    @Override
    public void getListUserStored(GetUserStoredCallBack callBack) {
        Timber.d("getListUserStored");
        compositeDisposable.add(usersDatabase.userDao().getAllUsers()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new ResourceSubscriber<List<UserEntity>>() {
                    @Override
                    public void onNext(List<UserEntity> entityList) {
                        callBack.onListUser(entityList);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.onError();
                    }

                    @Override
                    public void onComplete() {

                    }
                    /*@Override
                    public void onNext(List<UserEntity> entityList) {
                        callBack.onListUser(entityList);
                    }

                    @Override
                    public void onError(Throwable t) {
                        callBack.onError();
                    }

                    @Override
                    public void onComplete() {

                    }*/
                }));
    }

    @Override
    public void getUserViaGender(String gender, GetUserCalback callback) {
        Timber.d("getUserViaGender");
        GetUserViaGender job = new GetUserViaGender(gender);
        Single<UserModel> single = mSpJobManager.getListener(job);
        subscribeAndQueue(single, job, callback);
    }

    @Override
    public void storedUser(UserEntity userEntity, StoredDeleteUserCallback callBack) {
        Timber.d("storedUser");
        usersDatabase.userDao().inserUser(userEntity)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        callBack.onSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.onFail();
                    }
                });
    }

    @Override
    public void storedMultipleUser(List<UserEntity> listUsersEntity, StoredDeleteUserCallback callBack) {
        Timber.d("storedUser");
        usersDatabase.userDao().insertMultipleUser(listUsersEntity)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        callBack.onSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.onFail();
                    }
                });
    }

    @Override
    public void getUserViaSeed(String seed, GetUserCalback callback) {
        Timber.d("getUserViaSeed");
        GetUserViaSeed job = new GetUserViaSeed(seed);
        Single<UserModel> single = mSpJobManager.getListener(job);
        subscribeAndQueue(single, job, callback);
    }

    @Override
    public void deleteUser(UserEntity userEntity, StoredDeleteUserCallback callBack) {
        Timber.d("deleteUser");
        usersDatabase.userDao().deleteUser(userEntity.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        callBack.onSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.onFail();
                    }
                });
    }

    @Override
    public void deleteAllUser(StoredDeleteUserCallback callBack) {
        Timber.d("deleteAllUser");
        usersDatabase.userDao().deleteAllUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        callBack.onSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.onFail();
                    }
                });
    }

}
